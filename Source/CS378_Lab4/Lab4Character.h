// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab4Character.generated.h"

UENUM(BlueprintType)
enum class ECharacterActionStateEnum : uint8
{
	IDLE UMETA(DisplayName = "Idling"),
	MOVE UMETA(DisplayName = "Moving"),
	JUMP UMETA(DisplayName = "Jumping"),
	INTERACT UMETA(DisplayName = "Interacting")
};

UCLASS()
class CS378_LAB4_API ALab4Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab4Character();

	UFUNCTION(BlueprintImplementableEvent)
		void Move(float value);

	UFUNCTION(BlueprintImplementableEvent)
		void Strafe(float value);

	UFUNCTION(BlueprintImplementableEvent)
		void InteractionStarted();

	UFUNCTION(BlueprintImplementableEvent)
		void InteractionEnded();

	UFUNCTION(BlueprintImplementableEvent)
		void JumpStarted();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		ECharacterActionStateEnum CurrentActionState;

	UFUNCTION(BlueprintCallable)
		bool CanPerformAction(ECharacterActionStateEnum updatedAction);

	UFUNCTION(BlueprintCallable)
		void UpdateAction(ECharacterActionStateEnum newAction);
	
	UFUNCTION(BlueprintCallable)
		void ApplyMovement(float value);
	
	UFUNCTION(BlueprintCallable)
		void ApplyStrafe(float value);

	UFUNCTION(BlueprintCallable)
		void BeginInteraction();
	
	UFUNCTION(BlueprintCallable)
		void EndInteraction();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	FTimerHandle InteractTimerHandle;

};
