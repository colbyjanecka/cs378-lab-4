// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CS378_Lab4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ACS378_Lab4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	ACS378_Lab4GameModeBase();
	
};
