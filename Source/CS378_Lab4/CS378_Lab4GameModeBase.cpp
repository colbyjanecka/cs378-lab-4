// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab4GameModeBase.h"
#include "Lab4Character.h"
#include "Lab4PlayerController.h"

ACS378_Lab4GameModeBase::ACS378_Lab4GameModeBase()
{
	PlayerControllerClass = ALab4PlayerController::StaticClass();
	
	//set defualt pawn class for our character class
	static ConstructorHelpers::FObjectFinder<UClass> characterBPClass(TEXT("Class'/Game/Blueprints/Lab4CharacterBP.Lab4CharacterBP_C'"));

	if(characterBPClass.Object)
	{
		UClass* characterBP = (UClass*)characterBPClass.Object;
		DefaultPawnClass = characterBP;
	}
	else
	{
		DefaultPawnClass = ALab4Character::StaticClass();
	}
}


