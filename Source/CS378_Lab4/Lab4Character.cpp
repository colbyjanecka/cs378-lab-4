// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4Character.h"
#include "TimerManager.h"

// Sets default values
ALab4Character::ALab4Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ALab4Character::ApplyMovement(float value)
{
	AddMovementInput(GetActorForwardVector(), value);
}

void ALab4Character::ApplyStrafe(float value)
{
	AddMovementInput(GetActorRightVector(), value);
}

void ALab4Character::BeginInteraction()
{
	
	GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Green, FString::Printf(TEXT("setting interaction timer!")));
	GetWorldTimerManager().SetTimer(InteractTimerHandle, this, &ALab4Character::EndInteraction, 3.0f, false);
}

void ALab4Character::EndInteraction()
{
	GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Green, FString::Printf(TEXT("timer ended. ending interaction!")));
	CurrentActionState = ECharacterActionStateEnum::IDLE;
}

// Called when the game starts or when spawned
void ALab4Character::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ALab4Character::CanPerformAction(ECharacterActionStateEnum updatedAction)
{
	switch(CurrentActionState)
	{
	case ECharacterActionStateEnum::IDLE:
		return true;
	case ECharacterActionStateEnum::MOVE:
		if (updatedAction == ECharacterActionStateEnum::INTERACT)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Red, FString::Printf(TEXT("Can't Interact while Moving!")));
			return false;
		}
		return true;
		break;
	case ECharacterActionStateEnum::JUMP:
		if (updatedAction == ECharacterActionStateEnum::INTERACT)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Red, FString::Printf(TEXT("Can't Interact while Jumping!")));
			return false;
		}
		return true;
		break;
	case ECharacterActionStateEnum::INTERACT:
		if (updatedAction == ECharacterActionStateEnum::JUMP)
		{
			GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Red, FString::Printf(TEXT("Can't Move while Interacting!")));
		}
		return false;
	}
	return false;	
}

void ALab4Character::UpdateAction(ECharacterActionStateEnum newAction)
{
	if (newAction == ECharacterActionStateEnum::IDLE)
	{
		if (GetVelocity().IsNearlyZero())
		{
			CurrentActionState = ECharacterActionStateEnum::IDLE;
		}
		else
		{
			CurrentActionState = ECharacterActionStateEnum::MOVE;
		}
	}
	else
	{
		CurrentActionState = newAction;
	}
}

// Called every frame
void ALab4Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ALab4Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

