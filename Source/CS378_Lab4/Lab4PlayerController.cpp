// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4PlayerController.h"

#include "Lab4Character.h"

ALab4PlayerController::ALab4PlayerController()
{
	
}

void ALab4PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("Move", this, &ALab4PlayerController::Move);
	InputComponent->BindAxis("Strafe", this, &ALab4PlayerController::Strafe);
	InputComponent->BindAction("Jump", IE_Pressed, this, &ALab4PlayerController::Jump);
	InputComponent->BindAction("Interact", IE_Pressed, this, &ALab4PlayerController::InteractBegin);
	InputComponent->BindAction("Interact", IE_Released, this, &ALab4PlayerController::InteractEnded);
}

void ALab4PlayerController::Move(float value)
{
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->Move(value);
	}
}

void ALab4PlayerController::Strafe(float value)
{
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->Strafe(value);
	}
}

void ALab4PlayerController::Jump()
{
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->JumpStarted();
	}
}

void ALab4PlayerController::InteractBegin()
{
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->InteractionStarted();
	}
}

void ALab4PlayerController::InteractEnded()
{
	ALab4Character* character = Cast<ALab4Character>(this->GetCharacter());
	if (character)
	{
		character->InteractionEnded();
	}
}
