// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab4PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CS378_LAB4_API ALab4PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ALab4PlayerController();

	virtual void SetupInputComponent() override;

protected:

	UFUNCTION()
		void Move(float value);

	UFUNCTION()
		void Strafe(float value);

	UFUNCTION()
		void Jump();

	UFUNCTION()
		void InteractBegin();

	UFUNCTION()
		void InteractEnded();
};
